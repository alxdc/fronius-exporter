FROM python:3.8-slim AS compile-image

RUN mkdir /app
COPY /src/requirements.txt /app/requirements.txt
RUN pip install --user -r /app/requirements.txt

FROM python:3.8-slim
COPY --from=compile-image /root/.local /root/.local

COPY /src /app

ENV PATH=/root/.local/bin:$PATH
WORKDIR /app
ENTRYPOINT ["python", "-u", "/app/main.py"]