import requests
import yaml
import argparse
import os
import logging
import sys
from urllib3.exceptions import ConnectTimeoutError
from time import sleep

from influxdb_client import InfluxDBClient, Point
from influxdb_client.client.write_api import SYNCHRONOUS

DEFAULT_CONFIG_FILE = "config.yaml"

class FroniusExporter:

    def __init__(self, args):

        self.load_config(args)

        self.fields = {
            "PAC": {
                "name": "instant_power",
                "unit": "W"
            },
            "DAY_ENERGY": {
                "name": "day_energy",
                "unit": "Wh"
            },
            "TOTAL_ENERGY": {
                "name": "total_energy",
                "unit": "Wh"
            }
        }

        client = InfluxDBClient(url=self.config["influxdb"]["address"], token=self.config["influxdb"]["token"], org=self.config["influxdb"]["org"])
        self.write_api = client.write_api(write_options=SYNCHRONOUS)

    def setup_logger(self):
        self.logger = logging.getLogger("fronius_exporter")
        self.logger.setLevel(logging.DEBUG)

        # Console handler
        console_handle = logging.StreamHandler()
        console_handle.setLevel(logging.DEBUG)

        # Formatter
        formatter = logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")
        console_handle.setFormatter(formatter)

        self.logger.addHandler(console_handle)

        self.logger.info("Starting fronius exporter")

    def load_config(self, args):
        # Config priorities: --config arg > local config.yaml
        if args.config:
            config_file = args.config
        elif os.path.exists(DEFAULT_CONFIG_FILE):
            config_file = DEFAULT_CONFIG_FILE
        else:
            self.logger.error("No config arg, nor local config file. Stopping application.")
            sys.exit(1)

        with open(config_file, "r") as config_yaml:
            self.config = yaml.full_load(config_yaml)

    def process_data(self, response):
        if 'PAC' in response['Body']['Data']:
            for key in self.fields:

                if key in response['Body']['Data']:
                    value = int(response['Body']['Data'][key]['Value'])
                    self.logger.info(f"{self.fields[key]['name']}: {value} {self.fields[key]['unit']}")
                    p = Point("solar_pv").field(self.fields[key]['name'], value)
                    self.write_api.write(bucket=self.config["influxdb"]["bucket"], record=p)

        elif 'UDC' in response['Body']['Data']:
            self.logger.info(f"Current voltage is {response['Body']['Data']['UDC']['Value']} V. Not enough power to start.")
        else:
            self.logger.info(f"Couldn't fetch values. Reponse was {response['Body']['Data']}")

    def start(self):

        while True:
            try:
                response = requests.get(f"http://{self.config['inverter']['address']}/solar_api/v1/GetInverterRealtimeData.cgi?Scope=Device&DeviceId=1&DataCollection=CommonInverterData").json()
                self.process_data(response)
                sleep(10)
            except (requests.exceptions.ConnectTimeout, requests.exceptions.ConnectionError) as e:
                self.logger.warning("Inverter is offline. Sleeping for 5 minutes.")
                sleep(300)

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("--config", help="configuration file path")
    args = parser.parse_args()

    exporter = FroniusExporter(args)

    exporter.setup_logger()
    exporter.start()